# Prisma

# Prerequisites
Docker with WSL 2 Backend configured 
https://www.docker.com/products/docker-desktop

Python 3.8
https://www.python.org/downloads/release/python-389/

### First Time Install

## Step 0
Install python, making sure to install pip, add python to path and increase path limits (all python install options)
Install docker, following https://docs.docker.com/docker-for-windows/wsl/ to install WSL 2 Backend 
Open Docker

## Step 1 
Clone prisma to whichever directory you want to store and run the files from

## Step 2
In windows shell, navigate to the prisma directory and run the following. All commands following this should be run in the same manner
```
pip install pipenv 
curl https://pyenv.run | bash
```
Second command is an auto installer for Pyenv which does not work through Pip, https://github.com/pyenv/pyenv-installer.

this installs the tools necessary to create the virtual environment

Restart your shell afterwards.

## Step 3
Rename template.env to just .env and edit in the bot token and db password you want to use 
This file will not commit to github by design, and you should never rename and commit this as it will contain the bot token and DB password you use locally.

## Step 4
`pipenv install --dev`

This installs all the packages into a virtual environment, including the dev dependencies

## Step 5
`docker-compose up -d postgres`

This starts up the postgres database inside docker

## Step 6

`pipenv run alembic upgrade heads`

This configures all the tables inside the database

### Each time you run it

## Step 0 
Open docker, then in windows shell run:

`docker-compose up -d postgres`

this starts the docker database

## Step 1
`pipenv run python -m prisma`

this starts the bot 

A unified Discord moderation bot yay!
