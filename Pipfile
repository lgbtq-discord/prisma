[[source]]
name = "pypi"
url = "https://pypi.org/simple"
verify_ssl = true

[pipenv]
allow_prereleases = true

[requires]
python_version = "3.8"

[dev-packages]
# This is useful to make the bot's code available to external scripts, e.g. database
# migrations or tests.
prisma = {editable = true, path = "."}
black = "~= 19.10b0"
discord-stubs = {editable = true, path = "./discord-stubs"}
flake8 = "~= 3.8.3"
hypothesis = "~= 5.19.0"
isort = "~= 5.1.1"
mypy = "~= 0.782"
pytest = "~= 6.0.0rc1"
pytest-cov = "~= 2.10.0"
pytest-mock = "~= 3.1.1"
sqlalchemy-stubs = "~= 0.3"

[packages]
alembic = "~= 1.4.2"
"discord.py" = "~= 1.7.0"
discord-ext-menus = {editable = true, git = "https://github.com/Rapptz/discord-ext-menus",ref = "master"}
loguru = "~= 0.5.1"
psycopg2-binary = "~= 2.8.5"
pydantic = "~= 1.6"
sqlalchemy = "~= 1.3"
toml = "~= 0.10.0"
atomicwrites = "*"
win32-setctime = "~= 1.0.3"
