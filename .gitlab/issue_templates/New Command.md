
<!-- describe the general workings of the command here -->

## Usage

```
<!-- describe the syntax of the command here -->
```

## Existing Implementations

<!-- if there are a bots which already implements this command, you can mention it here (linking to code is a plus!) -->
<!-- you can remove this section if it does not apply -->

/label ~command ~feature
