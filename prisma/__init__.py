from pydantic import BaseSettings, SecretStr

from . import style
from .bot import Bot, Config
from .context import Context
from .errors import UserError

__all__ = ["Bot", "Context", "UserError"]

style.monkeypatch_messageable()


def load_extensions(config: Config, bot: Bot):
    for extension in config.extensions:
        bot.load_extension(extension)


class Settings(BaseSettings):
    CONFIG_PATH: str = "config.toml"
    DISCORD_TOKEN: SecretStr
    DB_URL: str
    DB_PASSWORD: SecretStr

    class Config:
        env_prefix = "PRISMA_"
