from discord import Colour

DEFAULT = Colour(0x2F3136)
RED = Colour(0xF25267)
ORANGE = Colour(0xFF9D26)
YELLOW = Colour(0xFAD932)
GREEN = Colour(0x3DCC6D)
BLUE = Colour(0x4583FF)
PURPLE = Colour(0xA045E6)
BLUEGREY = Colour(0xAFB6C4)
WHITE = Colour(0xFFFFFF)
OFFWHITE = Colour(0xEDEDED)
