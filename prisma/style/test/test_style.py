from hypothesis import given, infer

from .. import Colour, Icon, TitleMode, embed_factory


@given(icon=infer, colour=infer, title=infer, title_mode=infer)
def test_embed_should_have_colour(
    icon: Icon, colour: Colour, title: str, title_mode: TitleMode
):
    embed = embed_factory(icon, colour)(title, title_mode=title_mode)

    assert embed.colour == colour


@given(icon=infer, colour=infer, title=infer, title_mode=infer)
def test_title_should_be_in_embed(
    icon: Icon, colour: Colour, title: str, title_mode: TitleMode
):
    embed = embed_factory(icon, colour)(title, title_mode=title_mode)

    if title_mode == "normal":
        assert title in embed.title
    elif title_mode == "description":
        assert title in embed.description


@given(icon=infer, colour=infer, title=infer, title_mode=infer, description=infer)
def test_description_should_be_in_embed(
    icon: Icon, colour: Colour, title: str, title_mode: TitleMode, description: str
):
    embed = embed_factory(icon, colour)(
        title, description=description, title_mode=title_mode
    )

    assert description in embed.description


@given(icon=infer, colour=infer, title=infer, title_mode=infer)
def test_icon_should_be_in_embed(
    icon: Icon, colour: Colour, title: str, title_mode: TitleMode
):
    embed = embed_factory(icon, colour)(title, title_mode=title_mode)

    if title_mode == "normal":
        assert str(icon) in embed.title
    elif title_mode == "description":
        assert str(icon) in embed.description


@given(icon=infer, colour=infer, title=infer)
def test_title_empty_when_description_mode(icon: Icon, colour: Colour, title: str):
    embed = embed_factory(icon, colour)(title, title_mode="description")

    assert embed.title == ""
