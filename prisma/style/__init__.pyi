# This module has a separate stub file in order to provide proper signatures for
# error_embed etc., because those functions are all defined using a helper function.

from typing import Callable, Literal, Optional, Union

import discord

from .colours import Colour
from .icons import Icon

__all__ = ["Colour", "Icon"]

TitleMode = Union[Literal["normal"], Literal["description"]]

def embed_factory(icon: Icon, colour: Colour) -> Callable: ...
def success_embed(
    title: str, description: Optional[str] = None, title_mode: TitleMode = "normal"
) -> discord.Embed: ...
def info_embed(
    title: str, description: Optional[str] = None, title_mode: TitleMode = "normal"
) -> discord.Embed: ...
def warning_embed(
    title: str, description: Optional[str] = None, title_mode: TitleMode = "normal"
) -> discord.Embed: ...
def error_embed(
    title: str, description: Optional[str] = None, title_mode: TitleMode = "normal"
) -> discord.Embed: ...
def unknown_embed(
    title: str, description: Optional[str] = None, title_mode: TitleMode = "normal"
) -> discord.Embed: ...
def monkeypatch_messageable() -> None: ...
