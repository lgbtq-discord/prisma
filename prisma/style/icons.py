from typing import List, Tuple

import discord


class Icon(discord.PartialEmoji):
    """ A specific emoji used by the bot as a UI element.

    This is just a subclass of :class:`~discord.PartialEmoji` but with a nicer `__init__`.
    """

    def __init__(self, name: str, id: int, animated: bool = False):
        self.name = name
        self.id = id
        self.animated = animated
        # Existing methods might expect this to be defined.
        self._state = None

    def is_available(self, client: discord.Client) -> bool:
        return self in client.emojis


SUCCESS = Icon("green_check", 871626043335774228)
INFO = Icon("42a", 839216439038574603)
WARNING = Icon("42a", 839216439038574603)
ERROR = Icon("42a", 839216439038574603)
NOTICE = Icon("42a", 839216439038574603)
HELP = Icon("42a", 839216439038574603)
UNKNOWN = Icon("42a", 839216439038574603)
COOLDOWN = Icon("42a", 839216439038574603)
CANCEL = Icon("red_x", 871626056757559358)
INVALID = Icon("42a", 839216439038574603)
FORBIDDEN = Icon("42a", 839216439038574603)
NO_ACCESS = Icon("42a", 839216439038574603)
FIRST = Icon("42a", 839216439038574603)
PREVIOUS = Icon("42a", 839216439038574603)
NEXT = Icon("42a", 839216439038574603)
LAST = Icon("42a", 839216439038574603)
CLOSE = Icon("42a", 839216439038574603)
NUMPAD = Icon("42a", 839216439038574603)
TOGGLE_ON = Icon("42a", 839216439038574603)
TOGGLE_OFF = Icon("42a", 839216439038574603)
MEMBER_JOIN = Icon("42a", 839216439038574603)
MEMBER_VERIFY = Icon("42a", 839216439038574603)
MEMBER_LEAVE = Icon("42a", 839216439038574603)
MEMBER_KICK = Icon("42a", 839216439038574603)
MEMBER_BAN = Icon("42a", 839216439038574603)
MEMBER_MUTE = Icon("42a", 839216439038574603)
MEMBER_UNMUTE = Icon("42a", 839216439038574603)
EXT_SUCCESS = Icon("42a", 839216439038574603)
EXT_ERROR = Icon("42a", 839216439038574603)
EXT_UNKNOWN = Icon("42a", 839216439038574603)
PING = Icon("42a", 839216439038574603)
COG = Icon("42a", 839216439038574603)


def all_constants() -> List[Tuple[str, Icon]]:
    """ Return a (name, value) list of all icons defined as constants in this module.
    """
    return [(name, value) for name, value in globals().items() if isinstance(value, Icon)]


def check_available(client: discord.Client) -> None:
    """ Check if all icons defined in this module are available for the bot to use.
    """

    unavailable_icons = [
        name
        # Using globals() like this is a bit hacky, but "tidier" solutions that
        # explicitly group the predefined icons, like SimpleNamespace or a submodule, run
        # into the issues of bad autocomplete and circular imports, respectively.
        for name, value in all_constants()
        if not value.is_available(client)
    ]

    if len(unavailable_icons) > 0:
        raise Exception(
            "Found unavailable/misnamed icons: "
            + ", ".join(str(i) for i in unavailable_icons)
        )
