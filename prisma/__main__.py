import logging

import discord
import sqlalchemy
import toml
from loguru import logger

from prisma import Bot, Settings, load_extensions
from prisma.bot import Config


# This snippet is from:
# https://github.com/Delgan/loguru#entirely-compatible-with-standard-logging
class InterceptHandler(logging.Handler):
    def emit(self, record):
        # Get corresponding Loguru level if it exists
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        # Find the origin of the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1

        logger.opt(depth=depth, exception=record.exc_info).log(level, record.getMessage())


logging.basicConfig(handlers=[InterceptHandler()], level=0)

# (Snippet ends here.)


@logger.catch
def main():
    settings = Settings()
    config_dict = toml.load(settings.CONFIG_PATH)
    config = Config.parse_obj(config_dict)
    logger.info("Loaded settings and config")
    logger.info(config)

    intents = discord.Intents.default()
    intents.members = True

    bot = Bot(command_prefix=config.prefix, config=config, intents=intents)

    load_extensions(config, bot)
    full_url = sqlalchemy.engine.url.make_url(settings.DB_URL)
    full_url = full_url.set(password=settings.DB_PASSWORD.get_secret_value())


    db_engine = sqlalchemy.create_engine(full_url)
    logger.info("Created database engine")

    bot.run(settings.DISCORD_TOKEN.get_secret_value(), db_engine=db_engine)


if __name__ == "__main__":
    main()
