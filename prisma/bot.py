from __future__ import annotations

from typing import TYPE_CHECKING, List, Mapping

import discord
from discord.ext import commands
from pydantic import BaseModel
from sqlalchemy.engine import Engine

import prisma.db as db
from prisma.errors import UserError

if TYPE_CHECKING:
    from prisma.context import Context


class StaffRoles(BaseModel):
    admin: int
    moderator: int
    assistant: int
    helper: int

class UserRoles(BaseModel):
    verified: int
    optin: Mapping[str, int]

class RoleConfig(BaseModel):
    staff: StaffRoles
    user: UserRoles


class Config(BaseModel):
    prefix: str
    extensions: List[str]
    roles: RoleConfig
    cogs: Mapping[str, dict]
    testServer: int
    lgbtqServer: int
    testMode: bool


class Bot(commands.Bot):
    db_engine: Engine

    def __init__(self, *args, config: Config, **kwargs):
        self.config = config
        super().__init__(*args, **kwargs)

    # FIXME: This signature is incompatible with Client.run; just define a different
    # method instead.
    def run(self, token: str, *args, db_engine: Engine, **kwargs):
        self.db_engine = db_engine
        db.make_session.configure(bind=self.db_engine)
        super().run(token, *args, **kwargs)

    async def on_ready(self):
        from prisma.style import icons

        icons.check_available(self)

    async def process_commands(self, message: discord.Message):
        if message.author.bot:
            return

        with db.session_scope() as session:

            def make_ctx(**kwargs) -> Context:
                # avoid circular import
                from prisma.context import Context

                return Context(session=session, **kwargs)

            ctx = await self.get_context(message, cls=make_ctx)
            await self.invoke(ctx)

    async def on_command_error(self, ctx: Context, error: Exception):
        if isinstance(error, UserError):
            await ctx.send(embed=error.to_embed())
        elif isinstance(error, commands.CheckFailure):
            await ctx.error(title= "Permissions Error",description = "You do not have permission to use this command" )
        else:
            await super().on_command_error(ctx, error)
