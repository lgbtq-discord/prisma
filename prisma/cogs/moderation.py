from typing import Union

import discord
from discord.ext import commands

from prisma import Context, UserError, menus, style
from prisma.cogs import Cog, make_cog_setup
from prisma.util import mention_fallback, is_staff, is_perm_staff


class ModerationCog(Cog):
    @commands.command()
    @commands.guild_only()
    @commands.check(is_perm_staff)
    async def ban(
        self, ctx: Context, user_or_id: Union[discord.User, int], reason: str = None
    ):
        # TODO: extract this into a custom converter
        if isinstance(user_or_id, int):
            try:
                user = await self.bot.fetch_user(user_or_id)
            except discord.NotFound:
                raise UserError(f"User with ID {user_or_id} does not seem to exist.")
        else:
            user = user_or_id

        try:
            ban = await ctx.guild.fetch_ban(user)
            embed = style.warning_embed(
                f"User {mention_fallback(user)} was already banned.",
                title_mode="description",
            )
            embed.add_field(name="Reason", value=ban.reason)
            await ctx.send(embed=embed)

            # Discord doesn't really complain if you ban someone more than once, but
            # there's not really a use to it, so we stop the command here.
            return
        except discord.NotFound:
            # No existing ban was found, so we can continue.
            pass

        embed = style.info_embed(
            f"You are about to ban {mention_fallback(user)}, are you sure?",
            title_mode="description",
        )

        confirmation = await menus.Confirm(embed).prompt(ctx)

        if confirmation == menus.Confirmation.CONFIRM:
            await ctx.guild.ban(user, reason=reason)
            await ctx.success("User banned.")
        elif confirmation == menus.Confirmation.CANCEL:
            await ctx.error("Canceled.")

setup = make_cog_setup(ModerationCog)
