import discord
from discord.ext import commands
from pydantic import BaseModel
from prisma import Context, UserError, Settings, db, style, menus
from prisma.cogs import Cog, make_cog_setup
from prisma.util import is_staff, is_perm_staff, is_lgbtq_server, is_user
from prisma.bot import Config

import toml

class MessagingCog(Cog, name="Messaging"):
    #class Config(BaseModel):
    #    staffmail_channel_id: int
    #    modmail_channel_id: int
    #    lgbtqServer: int
        
    def __init__(self, client):
        self.client = client
        
    @commands.command()
    @commands.dm_only()
    # Make sure to check that they are in the guild!
    async def report(self, ctx: Context):

        ### INTERNAL FUNCTIONS ###
        def authorcheck(message):
                        return message.author == ctx.author

        messages = {"greeting" : "__**LGBTQ+ ModMail Function**__\n"+
                    "This function will deliver an anonymous message to server staff.  Please take this seriously.",
                    "instructions" : "Prisma will prompt you with questions to help triage this request.\n",
                    "recipient" : "**Would you like this message delivered to All Staff or Moderators & Admins only?**\n"+
                    "Please answer 'Staff' or 'Mods'",
                    "subject" : "**What is the subject of this message?**",
                    "message" : "**What message would you like to send?**"}
        
        timeout = 500
        server = self.client.get_guild(id=self.config.lgbtqServer)
        staffchannel = server.get_channel(884791670485446696)
        modchannel = server.get_channel(884791761560539196)
        member = server.get_member(ctx.message.author.id)
        if "User" not in [role.name for role in member.roles]:
            raise UserError("Permissions Error", 
                            "Only verified users may use this command!")
        modmail = []
        for item in messages:
            msgflg = False
            while not msgflg:
                await ctx.send(messages[item])
                if item in ["greeting", "instructions"]:
                    msgflg = True
                elif item in ["recipient"]:
                    response = await ctx.bot.wait_for('message', check=authorcheck, timeout=timeout)
                    if response.content.lower() not in ["staff", "mods"]:
                        await ctx.send("Please respond with either 'Staff' or 'Mods'")
                    else:
                        if response.content.lower() == "staff":
                            deliverychannel = staffchannel
                        else:
                            deliverychannel = modchannel
                        msgflg = True
                else:
                    modmail.append(await ctx.bot.wait_for('message', check=authorcheck, timeout=timeout))
                    msgflg = True
        message = f"**Report Submitted**\n **Subject:** {modmail[0].content}\n **Message:** {modmail[1].content}"
        try:
            embed = style.info_embed(
                "Confirm Message",
                message,
                title_mode="description")
            confirmation = await menus.Confirm(embed).prompt(ctx)
            if confirmation == menus.Confirmation.CONFIRM:
                await deliverychannel.send(message)
                await ctx.success(f"Message Sent!")
            elif confirmation == menus.Confirmation.CANCEL:
                await ctx.error("Sending Cancelled", "Message not sent.")    
        except Exception:
            raise UserError(f"Prisma Error",f"An error has occured.  Please try the command again.")

        
                
setup = make_cog_setup(MessagingCog)


