from datetime import datetime

import discord
from discord.ext import commands

from prisma import Context, style, Bot, Settings
from prisma.cogs import Cog, make_cog_setup
from prisma.bot import Config
from prisma.util import FriendlyTimeDelta, mention_fallback, is_staff, is_perm_staff
from prisma import db




from alembic import op

class BasicCog(Cog, name = "Basic"):

    @commands.command()
    async def ping(self, ctx: Context):

        """ Measure several kinds of latency between the bot and Discord.
        """
        now = datetime.utcnow()
        msg = await ctx.send("Pong!")
        creation_latency = abs(msg.created_at - now).total_seconds()
        embed = style.info_embed(title="Pong!", description="**Latency Info**")
        embed.add_field(
            name="Hearbeat latency", value=f"{ctx.bot.latency * 1000 :.0f} ms"
        )
        embed.add_field(
            name="Creation latency", value=f"{creation_latency * 1000 :.0f} ms"
        )
        await msg.edit(content=None, embed=embed)


    @commands.command()
    async def dbinfo(self, ctx: Context):
        version = ctx.db.execute("SELECT version();").first()[0]
        embed = discord.Embed(title="Database Info")
        embed.add_field(name="PostgreSQL Version", value=version)
        await ctx.send(embed=embed)

setup = make_cog_setup(BasicCog)

