import discord
import re
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from time import time
from copy import copy
from discord.ext import commands
from prisma import Context, UserError, style, menus, db
from prisma.bot import Config
from prisma.cogs import Cog, make_cog_setup

class PQLeaderboard(db.Model):
    '''Model class to represent the PQ leaderboard'''
    __tablename__ = "pqleaderboard"
    userid = sa.Column('user', sa.BigInteger, nullable=False, primary_key=True)
    score = sa.Column('sessionscore', sa.Integer, nullable=False)
    seasonscore = sa.Column('seasonscore', sa.Integer, nullable=False)


class PubQuizCog(Cog):
    # Class to encapulate Pub Quiz related commands

    
    def __init__(self,client):
        self.client = client
        # Trying this but not 100% sure it will work
        self.time = 13
        self.question = 1

    # Helper functions
    async def collectanswers(self, ctx: Context, tstop: int):
        '''Collect posted answers and the users that posted them'''
        checkbot = lambda x: (x.author != self.client.user)        
        pqchannel = ctx.message.channel
        botmsgtime = pqchannel.last_message.created_at
        responses = []
        tstart = time()
        while True:
            try:
                responses.append(await pqchannel.purge(after=botmsgtime, check=checkbot))
            except:
                pass
            ctime = time()
            if (ctime-tstart) > tstop:
                break
        answers = []
        for item in responses:
            if len(item)!=0:
                answers.extend([(x.author,x.content) for x in item])
        return answers

    async def postanswers(self, ctx: Context, answers): 
        '''Post an embed displaying the user's names+answers in the order they were submitted'''
        msg = []
        for item in answers:
            msg.append(f"**{item[0].name}**\n{item[1]}")
        embed = style.info_embed(
            "Pub Quiz Answers",
            "\n".join(msg),
            title_mode="description")        
        await ctx.send(embed=embed)
         
    async def postleaderboard(self, ctx: Context):
        '''Post an embed containing the quiz leaderboard'''
        # This should be a helper function because it is triggered by multiple commands
        msg = []
        players = ctx.db.query(PQLeaderboard).filter(PQLeaderboard.score!=0).order_by(sa.desc(PQLeaderboard.score)).all()
        for player in players:
            member = ctx.guild.get_member(player.userid)
            msg.append(f"**{member.name}**\n Has a total of **{player.score}** placing them in position **{players.index(player)+1}**. ({player.seasonscore} total points.)")
        embed = style.info_embed(
            "LGBTQ+Discord Pub Quiz Leaderboard",
            "\n".join(msg),
            title_mode="description"
        )
        await ctx.send(embed=embed)



    # Pub Quiz Command Group
    @commands.has_any_role('Quizmaster','prisma-dev')
    @commands.group(pass_context=True)
    async def pq(self, ctx: Context):
        if ctx.invoked_subcommand is None:
            embed = style.info_embed(
                "Pub Quiz Commands",
                title_mode="description")
            embed.add_field(name="`start`",value="Starts the Pub Quiz")
            embed.add_field(name="`q <question text>`",value="Submit quiz question")
            embed.add_field(name="`sq <question text>`", value="Submit super question")
            embed.add_field(name="`a <question answer>`",value="Post quiz answer")
            embed.add_field(name="`correct <list of @users>`",value="Record correct users")
            embed.add_field(name="`end`",value="Ends the Pub Quiz")
            embed.add_field(name="`leaderboard`",value="Displays the quiz leaderboard")
            embed.add_field(name="`settime <time (s)>`",value="Sets the question time")
            embed.add_field(name="`adjustscore <user> <value>`",value="Adjust user's score by `<value>`")
            await ctx.send(embed=embed)

    @pq.command(pass_context=True)
    async def start(self, ctx: Context):
        '''Start the pub quiz.  Posts announcement, sets question time to 13s, and starts 
        persistant leaderboard'''
        embed = style.info_embed(
            "Pub Quiz",
            title_mode="description")
        embed.add_field(name="The Pub Quiz is starting soon!",value="To recieve questions via DM use the command `<COMMAND NOT FOUND, BLAME AARD>`")
        await ctx.send(embed=embed)
        # NEED TO HAVE BOT DO THE FOLLOWING THINGS AT START:
        # -Set default question time - set as default attribute
        # -Start a question counter - set as default attribute
        # -Start a persistant leaderboard

    @pq.command(pass_context=True)
    async def q(self, ctx: Context, *question):
        '''Posts pq questions and handles answer collection for however long time is set for''' 
        # 1. Get Question time
        qtime = self.time
        # 2. Post Question/Question number
        embed = style.info_embed(
            "Pub Quiz",
            title_mode="description")
        fullquestion = " ".join(question)
        embed.add_field(name=f"Question {self.question}",value=fullquestion)
        await ctx.send(embed=embed)
        answers = await self.collectanswers(ctx, qtime)
        await self.postanswers(ctx, answers)

    @pq.command(pass_context=True)
    async def sq(self, ctx: Context, *question):
        '''Posts pq super question and handles answer collection for the sq time length'''
        # 1. Get Question time
        qtime = 20 # Hard code sq time
        # 2. Post Question/Question number
        embed = style.info_embed(
            "Pub Quiz",
            title_mode="description")
        fullquestion = " ".join(question)
        embed.add_field(name=f"Question {self.question}",value=fullquestion)
        await ctx.send(embed=embed)
        answers = await self.collectanswers(ctx, qtime)
        await self.postanswers(ctx, answers)

    @pq.command(pass_context=True)
    async def a(self, ctx: Context, *answer):
        '''Post Answer'''
        embed = style.info_embed(
            "Pub Quiz",
            title_mode="description")
        fullanswer = " ".join(answer)
        embed.add_field(name="The answer is...",value=fullanswer)
        self.question += 1
        await ctx.send(embed=embed)

    @pq.command(pass_context=True)
    async def correct(self, ctx: Context, *correct):
        '''Announces correct answers and adds points to users listed in the appropriate amounts/order''' 
        memids = []
        for member in correct:
            #print(type(member))
            #print(member)
            extractedid = "".join(re.findall(r'\d+', member))
            memids.append(int(extractedid))
            if len(ctx.db.query(PQLeaderboard).filter(PQLeaderboard.userid==memids[-1]).all())==0:
                entry = PQLeaderboard(userid=memids[-1],score=0,seasonscore=0)
                ctx.db.add(entry)
                ctx.db.flush()

        if len(memids) == 1:
            # Query user in table
            user = ctx.db.query(PQLeaderboard).filter(PQLeaderboard.userid==memids[0]).all()[0]
            user.score+=20
            user.seasonscore+=20
            ctx.db.commit()

        elif len(memids) > 1:
            i=1
            for memberid in memids:
                user = ctx.db.query(PQLeaderboard).filter(PQLeaderboard.userid==memberid).all()[0]
                if i==1:
                    user.score+=13
                    user.seasonscore+=13
                    ctx.db.commit()
                    i+=1
                elif i==2:
                    user.score+=12
                    user.seasonscore+=12
                    ctx.db.commit()
                    i+=1
                elif i==3:
                    user.score+=11
                    user.seasonscore+=11
                    ctx.db.commit()
                    i+=1
                else:
                    user.score+=10
                    user.seasonscore+=10
                    ctx.db.commit()
                    i+=1
        embed = style.info_embed(
            "Pub Quiz",
            title_mode="description")
        #members = [x.mention for x in correct]
        embed.add_field(name= "Correct",value="\n".join(correct))
        await ctx.send(embed=embed)

    @pq.command(pass_context=True)
    async def end(self, ctx: Context):
        '''End the pubquiz'''
        # Post the leaderboard as an embed
        await self.postleaderboard(ctx)
        # Clean up
        self.time = 13
        self.question = 1
        lbentries = ctx.db.query(PQLeaderboard).all()
        for item in lbentries:
            item.score=0
            ctx.db.commit()
        # Announce the end of the quiz
        embed = style.info_embed(
            "Pub Quiz",
            title_mode="description")
        embed.add_field(name="The Pub Quiz has ended",value="Thanks for playing!")
        await ctx.send(embed=embed)


    @pq.command(pass_context=True)
    async def leaderboard(self, ctx: Context):
        '''Posts the leaderboard as an embed in the channel'''
        await self.postleaderboard(ctx)

    @pq.command(pass_context=True)
    async def settime(self, ctx: Context, qtime: int):
        '''Set the question time manually'''
        # Update persistant time value
        self.time = qtime
        # Announce update
        embed = style.info_embed(
            "Pub Quiz",
            title_mode="description")
        embed.add_field(name="Question Time Updated",value=f"Time updated to **{self.time}** seconds.")
        await ctx.send(embed=embed)

    @pq.command(pass_context=True)
    async def adjustscore(self, ctx: Context, user: discord.Member, value: int):
        '''Adjust score of user by value (value should be signed so adjustment can occur through simple addition)'''
        player = ctx.db.query(PQLeaderboard).filter(PQLeaderboard.userid==user.id).all()[0]
        player.score+=value
        if player.score < 0:
            player.score = 0
        player.seasonscore+=value
        if player.seasonscore < 0:
            player.seasonscore = 0
        ctx.db.commit()
        embed = style.info_embed(
            "Pub Quiz",
            title_mode="description")
        embed.add_field(name=f"Player {user.name} score adjusted",value=value)
        await ctx.send(embed=embed)
    
setup = make_cog_setup(PubQuizCog)




