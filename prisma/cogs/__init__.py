from __future__ import annotations

from abc import ABCMeta
from typing import Callable, ClassVar, Optional, Type

import pydantic
from discord.ext import commands

from prisma import Bot


class CogABCMeta(commands.CogMeta, ABCMeta):
    pass


class Cog(commands.Cog, metaclass=CogABCMeta):
    Config: ClassVar[Optional[Type[pydantic.BaseModel]]] = None

    def __init__(self, bot: Bot, **kwargs) -> None:
        self.bot = bot

        if self.__class__.Config is None:
            # This cog doesn't need any config, so we're done.
            return

        if (config := kwargs.get("config")) is None:
            raise TypeError("missing config argument")

        self.config = config


def make_cog_setup(cls: Type[Cog]) -> Callable[[Bot], None]:
    def setup(bot: Bot) -> None:
        if cls.Config is not None:
            config = cls.Config.parse_obj(bot.config.cogs[cls.__name__])
            cog = cls(bot, config=config)
        else:
            cog = cls(bot)

        bot.add_cog(cog)

    return setup


def import_all_cogs():
    """Import all modules below this one (excluding tests).

    The main use for this is when you need to collect (via SQLAlchemy's metadata
    object) the tables corresponding to all models defined by a cog; this only happens
    once the model class definition is run, i.e. when the module containing it is
    imported.
    """
    # TODO: only import the cogs enabled in config.toml? This would have the benefit of
    # detecting a schema change if you enable a cog, but it may not be desirable to
    # generate table deletion if a cog is disabled.

    import importlib
    import pkgutil

    for _finder, name, _ispkg in pkgutil.iter_modules(__path__):
        if name == "test":
            # This is a test package, we don't want it.
            # TODO: consider moving tests to a separate toplevel directory entirely
            continue

        print(f"Importing {__name__}.{name}")
        importlib.import_module(f"{__name__}.{name}")
