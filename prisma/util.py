from datetime import timedelta
from typing import Dict, Tuple

import discord
from discord.ext import commands
from prisma.bot import Config
from prisma import Settings
import toml

class FriendlyTimeDelta(timedelta):
    """A Pydantic type that parses timedeltas from a dictionary instead of a string.

    Specifically, this allows for either plain `timedelta` values, or a dictionary of
    arguments to the timedelta constructor, i.e. of the form
    `{"weeks": ..., "hours": ..., ...}`.
    """

    @classmethod
    def __get_validators__(cls):
        yield cls.parse

    @classmethod
    def parse(cls, value):
        if isinstance(value, timedelta):
            return cls(
                days=value.days, seconds=value.seconds, microseconds=value.microseconds
            )

        if isinstance(value, dict):
            return cls(**value)

        # TODO: fallback to pydantic's default timedelta parsing?
        # TODO: human-friendly timedelta parsing?

        raise TypeError(f"Can't parse {cls.__name__} from a {type(value).__name__}")


def split_timedelta(
    td: timedelta,
    units: Tuple[str, ...] = (
        "weeks",
        "days",
        "hours",
        "minutes",
        "seconds",
        "milliseconds",
        "microseconds",
    ),
) -> Dict[str, int]:
    """Break a timedelta down into multiple units.

    >>> split_timedelta(timedelta(days=2), units=("weeks", "hours"))
    {'weeks': 0, 'hours': 48}
    >>> split_timedelta(timedelta(seconds=100000))
    {'weeks': 0, 'days': 1, 'hours': 3, 'minutes': 46,
     'seconds': 40, 'milliseconds': 0, 'microseconds': 0}
    """

    result = dict()

    for unit in units:
        amount, td = divmod(td, timedelta(**{unit: 1}))
        result[unit] = amount

    return result


def mention_fallback(user: discord.User) -> str:
    """
    Mention a user, but provide a fallback value in case the ID isn't loaded
    clientside.
    """

    return f"{user.mention} ({user})"


def is_perm_staff(ctx) -> bool:
    settings = Settings()
    config_dict = toml.load(settings.CONFIG_PATH)
    config = Config.parse_obj(config_dict)
    for role in ctx.author.roles:
        if role.id == config.roles.staff.admin or role.id == config.roles.staff.moderator or role.id == config.roles.staff.assistant:
            return True
    if(ctx.guild.id == config.testServer):
        return True
    return False


def is_staff(ctx) -> bool:
    settings = Settings()
    config_dict = toml.load(settings.CONFIG_PATH)
    config = Config.parse_obj(config_dict)
    for role in ctx.author.roles:
        if role.id == config.roles.staff.admin or role.id == config.roles.staff.moderator or role.id == config.roles.staff.assistant or role.id == config.roles.staff.helper:
            return True
    if(ctx.guild.id == config.testServer):
        return True
    return False


def is_user(ctx) -> bool:
    settings = Settings()
    config_dict = toml.load(settings.CONFIG_PATH)
    config = Config.parse_obj(config_dict)
    user_role = ctx.guild.get_role(config.roles.user.verified)
    for role in ctx.author.roles:
        if role.id == user_role:
            return True
    return False


def attachments_to_str(ctx) -> str:
    """
    Parses attachment urls and returns them as strings so they can be stored in warnings.
    If there are no attachments, it returns a blank string.
    """
    if len(ctx.message.attachments) == 0:
        return ''
    attach_list = []
    for attachment in ctx.message.attachments:
        attach_list.append(attachment.url)
    return ' '.join(attach_list)


def is_lgbtq_server(ctx) -> bool:
    settings = Settings()
    config_dict = toml.load(settings.CONFIG_PATH)
    config = Config.parse_obj(config_dict)
    if(ctx.guild.id == config.lgbtqServer):
        return True
    return False
