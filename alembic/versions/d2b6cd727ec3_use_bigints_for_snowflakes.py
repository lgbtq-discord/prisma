"""Use bigints for snowflakes

Revision ID: d2b6cd727ec3
Revises: 28937d9f66d2
Create Date: 2020-12-22 01:24:15.790778

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "d2b6cd727ec3"
down_revision = "28937d9f66d2"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("warnings", "user_id", type_=sa.BigInteger)
    op.alter_column("warnings", "issuer_id", type_=sa.BigInteger)


def downgrade():
    op.alter_column("warnings", "user_id", type_=sa.Integer)
    op.alter_column("warnings", "issuer_id", type_=sa.Integer)
