"""Add warning kind and issuer

Revision ID: 28937d9f66d2
Revises: 5b98f04d167f
Create Date: 2020-12-22 00:35:09.722038

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "28937d9f66d2"
down_revision = "5b98f04d167f"
branch_labels = None
depends_on = None

warning_kind_type = postgresql.ENUM("FORMAL", "VERBAL", "DBATC", name="warning_kind")


def upgrade():
    warning_kind_type.create(op.get_bind())

    op.add_column("warnings", sa.Column("issuer_id", sa.Integer(), nullable=False))
    op.add_column(
        "warnings", sa.Column("kind", warning_kind_type, nullable=False,),
    )


def downgrade():
    op.drop_column("warnings", "kind")
    op.drop_column("warnings", "issuer_id")

    warning_kind_type.drop(op.get_bind())
