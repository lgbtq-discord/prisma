"""Add alias table

Revision ID: a5b865d78b55
Revises: 7d58fb76c39f
Create Date: 2021-08-07 04:21:44.928446

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "a5b865d78b55"
down_revision = "7d58fb76c39f"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "alias",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column('rankid', sa.BigInteger, sa.ForeignKey('roles.rankid'), nullable=False),
        sa.Column('rankname', sa.Text, nullable=False),
        sa.PrimaryKeyConstraint("id"),
        )

def downgrade():
    op.drop_table("alias")

        
