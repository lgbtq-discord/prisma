"""create invite table

Revision ID: 79d2d5df4386
Revises: d2b6cd727ec3
Create Date: 2021-05-15 22:10:13.442379

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "79d2d5df4386"
down_revision = "d2b6cd727ec3"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "invites",
        sa.Column("origin", sa.Unicode(200), nullable = False),
        sa.Column("uses", sa.Integer(), nullable = False),
        sa.Column("inviteString", sa.Unicode(200), nullable = False, primary_key = True),
    )


def downgrade():
    op.drop_table("invites")
